//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "xenmgr.h"
#include <QtCore>
#include <QtDBus>

int
main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    xenmgr_t xenmgr(nullptr, nullptr, nullptr);

    app.exec();
}
