//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "dbus_listener.h"
#include <QtCore>
#include <QtDBus>
#include <memory>

int
main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    dbus_listener_t dbus_obj;

    app.exec();
}
