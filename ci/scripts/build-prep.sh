#!/bin/bash -xe

git clone https://gitlab.com/vglass/ivc.git
cd ivc
git checkout ${BRANCH}
PREFIX=/usr make install_user
cd ..

git clone https://gitlab.com/vglass/pv-display-helper.git
cd pv-display-helper
git checkout ${BRANCH}
make DESTDIR=/usr libraries_install
cd ..

git clone https://github.com/OpenXT/libxenbackend.git
cd libxenbackend
autoreconf -i .
./configure --prefix=/usr
make
make install
cd ..
