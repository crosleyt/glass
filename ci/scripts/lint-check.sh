#!/bin/bash

if [[ -n $(cat lint-output) ]]; then
	exit 255
fi

exit 0
