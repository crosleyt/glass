#include "vnc_translate.h"

std::unordered_map<rfbKeySym, uint32_t> vnc_key_table =
    {
        // Uppercase letters
        {XK_A, KEY_A},
        {XK_B, KEY_B},
        {XK_C, KEY_C},
        {XK_D, KEY_D},
        {XK_E, KEY_E},
        {XK_F, KEY_F},
        {XK_G, KEY_G},
        {XK_H, KEY_H},
        {XK_I, KEY_I},
        {XK_J, KEY_J},
        {XK_K, KEY_K},
        {XK_L, KEY_L},
        {XK_M, KEY_M},
        {XK_N, KEY_N},
        {XK_O, KEY_O},
        {XK_P, KEY_P},
        {XK_Q, KEY_Q},
        {XK_R, KEY_R},
        {XK_S, KEY_S},
        {XK_T, KEY_T},
        {XK_U, KEY_U},
        {XK_V, KEY_V},
        {XK_W, KEY_W},
        {XK_X, KEY_X},
        {XK_Y, KEY_Y},
        {XK_Z, KEY_Z},

        // Lowercase letters
        // Note that there is no "lowercase" as far as libinput
        // is concerned. Thus the lowercase vnc keys get mapped to
        // the same values as the uppercase letters.
        {XK_a, KEY_A},
        {XK_b, KEY_B},
        {XK_c, KEY_C},
        {XK_d, KEY_D},
        {XK_e, KEY_E},
        {XK_f, KEY_F},
        {XK_g, KEY_G},
        {XK_h, KEY_H},
        {XK_i, KEY_I},
        {XK_j, KEY_J},
        {XK_k, KEY_K},
        {XK_l, KEY_L},
        {XK_m, KEY_M},
        {XK_n, KEY_N},
        {XK_o, KEY_O},
        {XK_p, KEY_P},
        {XK_q, KEY_Q},
        {XK_r, KEY_R},
        {XK_s, KEY_S},
        {XK_t, KEY_T},
        {XK_u, KEY_U},
        {XK_v, KEY_V},
        {XK_w, KEY_W},
        {XK_x, KEY_X},
        {XK_y, KEY_Y},
        {XK_z, KEY_Z},

        // Numbers
        {XK_1, KEY_1},
        {XK_2, KEY_2},
        {XK_3, KEY_3},
        {XK_4, KEY_4},
        {XK_5, KEY_5},
        {XK_6, KEY_6},
        {XK_7, KEY_7},
        {XK_8, KEY_8},
        {XK_9, KEY_9},
        {XK_0, KEY_0},

        // A similar rule applies to the punctuation keys.
        // Since there is no concept of case, any given libinput
        // punctuation key represents both the regular key and
        // its "SHIFTed" counterpart. Thus, we map both to the
        // same key.

        // Punctuation (number keys)
        {XK_exclam, KEY_1},
        {XK_at, KEY_2},
        {XK_numbersign, KEY_3},
        {XK_dollar, KEY_4},
        {XK_percent, KEY_5},
        {XK_asciicircum, KEY_6},
        {XK_ampersand, KEY_7},
        {XK_asterisk, KEY_8},
        {XK_parenleft, KEY_9},
        {XK_parenright, KEY_0},

        // Punctuation (remainder)
        {XK_apostrophe, KEY_APOSTROPHE},
        {XK_asciitilde, KEY_GRAVE},
        {XK_backslash, KEY_BACKSLASH},
        {XK_bar, KEY_BACKSLASH},
        {XK_braceleft, KEY_LEFTBRACE},
        {XK_braceright, KEY_RIGHTBRACE},
        {XK_bracketleft, KEY_LEFTBRACE},
        {XK_bracketright, KEY_RIGHTBRACE},
        {XK_colon, KEY_SEMICOLON},
        {XK_comma, KEY_COMMA},
        {XK_equal, KEY_EQUAL},
        {XK_grave, KEY_GRAVE},
        {XK_greater, KEY_DOT},
        {XK_less, KEY_COMMA},
        {XK_minus, KEY_MINUS},
        {XK_period, KEY_DOT},
        {XK_plus, KEY_EQUAL},
        {XK_question, KEY_SLASH},
        {XK_quotedbl, KEY_APOSTROPHE},
        {XK_semicolon, KEY_SEMICOLON},
        {XK_slash, KEY_SLASH},
        {XK_underscore, KEY_MINUS},

        // Function keys
        {XK_F1, KEY_F1},
        {XK_F2, KEY_F2},
        {XK_F3, KEY_F3},
        {XK_F4, KEY_F4},
        {XK_F5, KEY_F5},
        {XK_F6, KEY_F6},
        {XK_F7, KEY_F7},
        {XK_F8, KEY_F8},
        {XK_F9, KEY_F9},

        // Special keys
        {XK_Alt_L, KEY_LEFTALT},
        {XK_Alt_R, KEY_RIGHTALT},
        {XK_BackSpace, KEY_BACKSPACE},
        {XK_Caps_Lock, KEY_CAPSLOCK},
        {XK_Control_L, KEY_LEFTCTRL},
        {XK_Control_R, KEY_RIGHTCTRL},
        {XK_Delete, KEY_DELETE},
        {XK_Down, KEY_DOWN},
        {XK_End, KEY_END},
        {XK_Escape, KEY_ESC},
        {XK_Home, KEY_HOME},
        {XK_Insert, KEY_INSERT},
        {XK_Left, KEY_LEFT},
        {XK_Num_Lock, KEY_NUMLOCK},
        {XK_Page_Down, KEY_PAGEDOWN},
        {XK_Page_Up, KEY_PAGEUP},
        {XK_Return, KEY_ENTER},
        {XK_Right, KEY_RIGHT},
        {XK_Scroll_Lock, KEY_SCROLLLOCK},
        {XK_Shift_L, KEY_LEFTSHIFT},
        {XK_Shift_R, KEY_RIGHTSHIFT},
        {XK_space, KEY_SPACE},
        {XK_Tab, KEY_TAB},
        {XK_Up, KEY_UP}

        // TODO: keypad keys
};

uint16_t
vnc_key_translate(rfbKeySym key)
{
    if (vnc_key_table.count(key)) {
        return vnc_key_table[key];
    }

    return 0;
}
