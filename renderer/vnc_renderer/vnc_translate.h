#ifndef VNC_TRANSLATE__H
#define VNC_TRANSLATE__H

#include <linux/input-event-codes.h>
#include <rfb/keysym.h>
#include <rfb/rfb.h>
#include <unordered_map>

#define VNC_LEFT_CLICK_BIT 0
#define VNC_RIGHT_CLICK_BIT 2
#define VNC_SCROLL_UP_BIT 4
#define VNC_SCROLL_DOWN_BIT 5

// Translate a VNC key code to a key code that libinput understands

extern std::unordered_map<rfbKeySym, uint32_t> vnc_key_table;
uint16_t
vnc_key_translate(rfbKeySym key);

#endif // VNC_TRANSLATE__H
