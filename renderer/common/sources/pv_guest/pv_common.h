//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_COMMON__H
#define PV_COMMON__H
#include <QDebug>

#define __TRACE__ qDebug() << __func__ << ": " << __LINE__;

#endif
