#ifndef BACKLIGHT_H
#define BACKLIGHT_H

//
// Backlight
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <QtCore>
#include <cstdint>   // uint32_t
#include <libudev.h> // udev, udev_device
#include <memory>    // std::unique_ptr

// ============================================================================
// Backlight Definition
// ============================================================================

enum class value_t : uint8_t {
    BRIGHTNESS,
    MAX_BRIGHTNESS
};

class backlight_t : public QObject
{
    Q_OBJECT

public:
    backlight_t();
    backlight_t(const uint32_t level);
    virtual ~backlight_t();

public slots:

    virtual uint32_t level() const;
    virtual void set_level(uint32_t level);

    virtual void increase(uint32_t step = 10);
    virtual void decrease(uint32_t step = 10);

private:
    virtual uint32_t value(const value_t v) const;
    virtual std::string to_string(const value_t v) const;

private:
    uint32_t m_max{0};
    uint32_t m_level{0};

    std::unique_ptr<struct udev, void (*)(struct udev *)> m_udev;
    std::unique_ptr<struct udev_device, void (*)(struct udev_device *)> m_udev_device;
};

#endif // BACKLIGHT_H
