//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef MODE__H
#define MODE__H

#include <QDebug>
#include <QSize>
#include <QString>
#include <memory>

extern "C" {
#include <drm/drm_mode.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
}

class drm_mode_t
{
public:
    explicit drm_mode_t(drmModeModeInfo *modeinfo);
    virtual ~drm_mode_t();

    virtual QSize resolution(void) const;
    virtual int32_t resolution_width(void) const;
    virtual int32_t resolution_height(void) const;

    virtual int32_t refresh_rate(void) const;

    virtual QString name(bool filtered) const;

    virtual drmModeModeInfo *modeinfo(void) const;

    bool operator>(const drm_mode_t &rhs)
    {
        return ((resolution_width() > rhs.resolution_width() &&
                 resolution_height() > rhs.resolution_height()) ||
                (resolution_width() > rhs.resolution_width() &&
                 resolution_height() == rhs.resolution_height()) ||
                (resolution_width() == rhs.resolution_width() &&
                 resolution_height() > rhs.resolution_height()));
    }

    bool operator>(QSize &rhs)
    {
        return ((resolution_width() > rhs.width() &&
                 resolution_height() > rhs.height()) ||
                (resolution_width() > rhs.width() &&
                 resolution_height() == rhs.height()) ||
                (resolution_width() == rhs.width() &&
                 resolution_height() > rhs.height()));
    }

    bool operator<(const drm_mode_t &rhs)
    {
        return ((resolution_width() < rhs.resolution_width() &&
                 resolution_height() < rhs.resolution_height()) ||
                (resolution_width() < rhs.resolution_width() &&
                 resolution_height() == rhs.resolution_height()) ||
                (resolution_width() == rhs.resolution_width() &&
                 resolution_height() < rhs.resolution_height()));
    }

    bool operator<(const QSize &rhs)
    {
        return ((resolution_width() < rhs.width() &&
                 resolution_height() < rhs.height()) ||
                (resolution_width() < rhs.width() &&
                 resolution_height() == rhs.height()) ||
                (resolution_width() == rhs.width() &&
                 resolution_height() < rhs.height()));
    }

    bool operator<=(const drm_mode_t &rhs)
    {
        return (*this < rhs) || (*this == rhs);
    }

    bool operator>=(const drm_mode_t &rhs)
    {
        return (*this > rhs) || (*this == rhs);
    }

    bool operator!=(const drm_mode_t &rhs)
    {
        return (resolution() != rhs.resolution());
    }

    bool operator==(const drm_mode_t &rhs)
    {
        return (resolution() == rhs.resolution());
    }

private:
    drmModeModeInfo *m_modeinfo;
};

#endif // MODE__H
