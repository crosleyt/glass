//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PT_GPU__H
#define PT_GPU__H

#include <desktop_plane.h>
#include <glass_types.h>
#include <gpu.h>

#include "pt_display.h"

class pt_gpu_t : public gpu_t
{
public:
    pt_gpu_t(json &gpu_config, std::shared_ptr<plane_t> parent);
    virtual ~pt_gpu_t() = default;

    virtual std::shared_ptr<desktop_plane_t> desktop();
    virtual qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> pinned_desktops();

    virtual void disable_display(display_plane_t &display);
    virtual void enable_display(display_plane_t &display);

private:
    qhash_t<uuid_t, std::shared_ptr<desktop_plane_t>> m_pinned_desktops;
};

#endif // PT_GPU__H
