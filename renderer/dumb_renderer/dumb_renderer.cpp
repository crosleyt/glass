//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "dumb_renderer.h"

#include <QCursor>
#include <QFontDatabase>
#include <QTimer>
#include <fstream>

extern int32_t g_banner_height;

dumb_renderer_t::dumb_renderer_t(window_manager_t &wm) : renderer_t(wm)
{
}

dumb_renderer_t::dumb_renderer_t(window_manager_t &wm,
                                 std::string display_config_path) : renderer_t(wm, display_config_path)
{
}

dumb_renderer_t::dumb_renderer_t(window_manager_t &wm,
                                 json &display_config) : renderer_t(wm, display_config)
{
}
