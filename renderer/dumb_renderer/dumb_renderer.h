//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DUMB_RENDERER__H
#define DUMB_RENDERER__H

#include <glass_types.h>

#include <renderer.h>
#include <text_overlay.h>
#include <window_manager.h>

#include <QObject>
#include <drm/drm_gpu.h>

#include "pt_gpu/pt_gpu.h"
#include "pv_guest/pv_desktop_resource.h"
#include "vglass_dbus_proxy.h"

using json = nlohmann::json;

class dumb_renderer_t : public renderer_t
{
    Q_OBJECT
public:
    explicit dumb_renderer_t(window_manager_t &wm);
    dumb_renderer_t(window_manager_t &wm, std::string display_config_path);
    dumb_renderer_t(window_manager_t &wm, json &display_config);

    virtual ~dumb_renderer_t() = default;

protected:
    virtual void pre_render_display(desktop_plane_t *, display_plane_t *) {}
    virtual void post_render_display(desktop_plane_t *, display_plane_t *) {}
};

#endif // DUMB_RENDERER__H
