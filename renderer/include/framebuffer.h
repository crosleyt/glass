//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef FRAMEBUFFER__H
#define FRAMEBUFFER__H
#include <QImage>
#include <glass_types.h>

class framebuffer_t
{
public:
    framebuffer_t(const QSize &size) : m_unique_id(0),
                                       m_width(size.width()),
                                       m_height(size.height()),
                                       m_stride(4 * size.width()),
                                       m_handle(0),
                                       m_vaddr(nullptr)
    {
    }

    framebuffer_t(uint32_t id,
                  uint32_t w,
                  uint32_t h,
                  uint32_t stride,
                  uint32_t handle,
                  uint8_t *vaddr) : m_unique_id(id),
                                    m_width(w),
                                    m_height(h),
                                    m_stride(stride),
                                    m_handle(handle),
                                    m_vaddr(vaddr),
                                    m_image(std::make_shared<QImage>((unsigned char *) vaddr, w, h, stride, QImage::Format_RGB32))
    {
    }

    virtual ~framebuffer_t() = default;

    virtual uint32_t unique_id() const
    {
        return m_unique_id;
    }

    virtual uint32_t width() const
    {
        return m_width;
    }

    virtual uint32_t height() const
    {
        return m_height;
    }

    virtual uint32_t stride() const
    {
        return m_stride;
    }

    virtual uint32_t length() const
    {
        return m_stride * m_height;
    }

    virtual uint32_t handle() const
    {
        return m_handle;
    }

    virtual size_t size() const
    {
        return length();
    }

    virtual rect_t rect() const
    {
        return rect_t(0, 0, m_width, m_height);
    }

    virtual std::shared_ptr<QImage> image()
    {
        return m_image;
    }

    virtual void flush(const region_t &region)
    {
        (void) region;
    }

protected:
    uint32_t m_unique_id;
    uint32_t m_width;
    uint32_t m_height;
    uint32_t m_stride;
    uint32_t m_handle;
    uint8_t *m_vaddr;
    std::shared_ptr<QImage> m_image;
};

#endif // FRAMEBUFFER__H
