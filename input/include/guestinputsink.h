//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef GUEST_INPUT_SINK__H
#define GUEST_INPUT_SINK__H

#include <xt_input_global.h>

extern bool input_debug_enabled;

class guest_input_sink_t : public QObject
{
    Q_OBJECT

public:
    guest_input_sink_t();
    virtual ~guest_input_sink_t() {}

    bool is_stubdom();

    bool absolute_enabled() { return m_absolute_enabled; }

signals:

    void error(QString errstr, uint32_t err);
    void keyboard_led_changed(uint32_t led_code);
public slots:

    virtual void enqueue_input_event(xt_input_event event) = 0;

protected:
    int32_t relZ_float_to_int(float relZ);
    bool m_absolute_enabled{false};

private:
    float m_relZ_fractional{0};
};

#endif //GUEST_INPUT_SINK__H
