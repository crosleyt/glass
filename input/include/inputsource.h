//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUT_SOURCE__H
#define INPUT_SOURCE__H

#include <QObject>
#include <memory>
#include <xt_input_global.h>
/*
    This is the interface for an 'InputSource' that generates input
    events for that input sources 'HostInputFilter'. The combination
    of the 'input source' and the 'HostInputFilter' take input from
    a given api, and get it ready for distribution to OpenXT guests.

    An implementation of the 'InputSource' should really only get
    input events, and leave processing of the events to the
    'HostInputFilter', where they can be transformed into an
    intermediate form before final transformation into the sink
    form (whatever the guest driver is expecting input to look like).

    The sourceType is a unique identifier for the implementation of
    the 'InputSource', and should match up with the 'HostInputFilter'
    type.

    Example sources could be libinput, VNC, RDP, maybe even X11.
    libinput is the first implementation, and can serve as an example.

*/

class vm_input_t;

class input_source_t : public QObject
{
    Q_OBJECT

public:
    input_source_t(void);
    virtual ~input_source_t(void);

signals:

    void input_event(void *event);
    void forward_filtered_input_event(struct xt_input_event event, std::shared_ptr<vm_input_t> target_vm = NULL);

public slots:

    virtual void set_led_code(const uint32_t led_code) = 0;

protected:
    int32_t m_source_type;
};

#endif //INPUT_SOURCE__H
