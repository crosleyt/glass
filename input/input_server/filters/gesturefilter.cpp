//
// Glass Display
//
// Copyright (C) 2016 - 2020 Assured Information Security, Inc. All rights reserved.
//
#include <gesturefilter.h>
#include <inputserverfilter.h>

// minimum number of events in each queue before we pay attention to them
const int32_t gesture_filter_t::vector_size = 5;

// should be a power of ten to compensate for integer math
const int32_t gesture_filter_t::granularity = 10;

//threshold in the direction of travel
const int32_t gesture_filter_t::direction_threshold = 4;

const int32_t MAX_NEG_INT = std::numeric_limits<int32_t>::min();

gesture_filter_t::gesture_filter_t(window_manager_t &wm,
                                   std::shared_ptr<input_action_t> touch_count_action,
                                   std::shared_ptr<input_action_t> gesture_action,
                                   std::shared_ptr<input_action_t> untouch_count_action) : m_wm(wm),
                                                                                           m_touch_count_action(touch_count_action),
                                                                                           m_gesture_action(gesture_action),
                                                                                           m_untouch_count_action(untouch_count_action)
{
    this->set_name("gesture_filter");
}

gesture_filter_t::~gesture_filter_t(void)
{
}

QPair<point_t, point_t>
gesture_filter_t::generate_signal_vector(struct Gesture &gesture)
{
    int32_t x_dir1 = 0, y_dir1 = 0;
    int32_t x_dist1 = 0, y_dist1 = 0;

    // This is being checked before the call, so it shouldn't be needed
    if (gesture.touch_count != gesture.slot_to_signal.size() || gesture.touch_count != gesture.trigger.finger_count) {
        return qMakePair(point_t(0, 0), point_t(0, 0));
    }

    int32_t usable_size = MAX_NEG_INT;
    for (auto signal : gesture.slot_to_signal.values()) {
        int32_t size = signal.size();

        usable_size = (usable_size == MAX_NEG_INT) ? size : std::min(size, usable_size);
    }

    if (usable_size < vector_size) {
        return qMakePair(point_t(0, 0), point_t(0, 0));
    }

    // calculate average of average of differentials for direction and
    // average of distances for distance
    for (auto signal : gesture.slot_to_signal.values()) {
        int32_t size = signal.size();
        int32_t x_dir2 = 0, y_dir2 = 0;

        for (int32_t i = size - usable_size + 1; i < size; i++) {
            auto p1 = signal.at(i - 1);
            auto p2 = signal.at(i);

            x_dir2 += ((p2.x() - p1.x()) * granularity);
            y_dir2 += ((p2.y() - p1.y()) * granularity);
        }

        x_dir2 /= usable_size;
        y_dir2 /= usable_size;
        x_dir1 += x_dir2;
        y_dir1 += y_dir2;

        x_dist1 += signal.last().x() - signal.first().x();
        y_dist1 += signal.last().y() - signal.first().y();
    }

    x_dir1 /= gesture.touch_count;
    y_dir1 /= gesture.touch_count;
    x_dist1 /= gesture.slot_to_signal.size();
    y_dist1 /= gesture.slot_to_signal.size();

    return qMakePair(point_t(x_dir1, y_dir1), point_t(x_dist1, y_dist1));
}

int32_t
gesture_filter_t::distance_threshold(const struct Gesture &gesture)
{
    switch (gesture.trigger.direction) {
        case DIRECTION_UP:
        case DIRECTION_DOWN:
            return m_wm.input_plane()->rect().width() / 8.6;
            break;
        case DIRECTION_LEFT:
        case DIRECTION_RIGHT:
            return m_wm.input_plane()->rect().height() / 8.6;
            break;
        case DIRECTION_ERROR:
            break;
    }
    return 0;
}

bool
gesture_filter_t::filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event *event)
{
    // Not using the vm
    (void) vm;

    // If there is not event then exit
    if (event == NULL) {
        return false;
    }

    // If this isn't a relative event, we dont have any filtering to do.
    if (event->type != XT_TYPE_TOUCH_DOWN &&
        event->type != XT_TYPE_TOUCH_UP &&
        event->type != XT_TYPE_TOUCH_MOVE) {
        return false;
    }

    // Iterate over the list of gestures
    for (int32_t i = 0; i < m_triggers.size(); i++) {
        Gesture &gesture = m_triggers[i];

        Q_ASSERT(gesture.trigger.type != TYPE_ERROR);

        // Currently only support the swipe type
        if (gesture.trigger.type != TYPE_SWIPE) {
            continue;
        }

        Q_ASSERT(gesture.trigger.direction != DIRECTION_ERROR);

        switch (event->type) {
            case XT_TYPE_TOUCH_DOWN: {
                // Make sure these always start off cleared, shouldn't ever be needed
                if (gesture.touch_count == 0) {
                    gesture.slot_to_signal.clear();
                    gesture.entered = false;
                    gesture.fired = false;
                }

                gesture.touch_count++;

                // Clear the signal, as we have a touchdown
                gesture.slot_to_signal.remove(event->flags);
                //gesture.slot_to_signal[event->flags] += point_t(event->absX, event->absY);
                gesture.slot_to_signal[event->flags] += point_t(event->data.touch.absX, event->data.touch.absY);

                if (gesture.entered && gesture.touch_count == gesture.trigger.finger_count + 1) {
                    emit filter_fire(m_untouch_count_action);
                    gesture.entered = false;
                    gesture.fired = false;
                }

                break;
            }

            case XT_TYPE_TOUCH_MOVE: {
                if (gesture.touch_count != 0) {
                    gesture.slot_to_signal[event->flags].append(point_t(event->data.touch.absX, event->data.touch.absY));

                    QPair<point_t, point_t> vector = generate_signal_vector(gesture);

                    if (vector.first != point_t(0, 0) || vector.second != point_t(0, 0)) {
                        if ((gesture.touch_count == gesture.trigger.finger_count) && !gesture.fired) {

                            //determine direction
                            int32_t right_vector = (((gesture.trigger.direction == DIRECTION_LEFT) || (gesture.trigger.direction == DIRECTION_RIGHT))
                                                        ? vector.first.x()
                                                        : vector.first.y());
                            int32_t wrong_vector = (((gesture.trigger.direction == DIRECTION_LEFT) || (gesture.trigger.direction == DIRECTION_RIGHT))
                                                        ? vector.first.y()
                                                        : vector.first.x());

                            if (((gesture.trigger.direction == DIRECTION_LEFT) || (gesture.trigger.direction == DIRECTION_UP)) ? right_vector < 0 : right_vector > 0) {
                                if ((qAbs(right_vector) >= direction_threshold) && (qAbs(wrong_vector) <= qAbs(right_vector) / 2)) {
                                    if (!gesture.entered) {
                                        emit filter_fire(m_touch_count_action);
                                        gesture.entered = true;
                                    }

                                    // determine distance
                                    if (gesture.entered && qAbs(((gesture.trigger.direction == DIRECTION_LEFT) || (gesture.trigger.direction == DIRECTION_RIGHT)) ? vector.second.x() : vector.second.y()) >= distance_threshold(gesture)) {
                                        emit filter_fire(m_gesture_action);
                                        gesture.fired = true;
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                break;
            }

            case XT_TYPE_TOUCH_UP: {
                // Prevent touch_count underflowing to max int
                if (gesture.touch_count) {
                    gesture.touch_count--;
                    gesture.slot_to_signal.remove(event->flags);

                    if (gesture.entered && gesture.touch_count == gesture.trigger.finger_count - 1) {
                        emit filter_fire(m_untouch_count_action);
                        gesture.entered = false;
                        gesture.fired = false;
                        return true;
                    }
                }

                // Final bookkeeping, shouln't ever be needed
                if (gesture.touch_count == 0) {
                    if (gesture.entered) {
                        emit filter_fire(m_untouch_count_action);
                    }
                    gesture.slot_to_signal.clear();
                    gesture.entered = false;
                    gesture.fired = false;
                }

                break;
            }
        }
    }

    return false;
}

bool
gesture_filter_t::set_triggers(json triggers)
{
    clear_triggers();

    // Parse the config and add all gestures to the list of triggers
    for (auto trigger : triggers) {
        add_trigger(trigger);
    }

    return true;
}

bool
gesture_filter_t::clear_triggers(void)
{
    m_triggers.clear();
    return true;
}

Type
evaluate_type(const QString type)
{
    if (type == "swipe") {
        return TYPE_SWIPE;
    } else if (type == "edge_swipe") {
        return TYPE_EDGE_SWIPE;
    } else if (type == "tap_and_hold") {
        return TYPE_TAP_AND_HOLD;
    } else if (type == "double_tap") {
        return TYPE_DOUBLE_TAP;
    } else {
        return TYPE_ERROR;
    }
}

Direction
evaluate_direction(const QString direction)
{
    if (direction == "left") {
        return DIRECTION_LEFT;
    } else if (direction == "right") {
        return DIRECTION_RIGHT;
    } else if (direction == "up") {
        return DIRECTION_UP;
    } else if (direction == "down") {
        return DIRECTION_DOWN;
    } else {
        return DIRECTION_ERROR;
    }
}

bool
gesture_filter_t::add_trigger(json trigger)
{
    if ((trigger.find("disabled") != trigger.end()) && (trigger["disabled"].get<bool>() == true)) {
        return false;
    }

    Gesture gesture;
    int32_t finger_count = trigger["finger_count"];
    std::string gesture_type = trigger["gesture_type"];
    std::string gesture_direction = trigger["gesture_direction"];

    // Get the type of Gesture, and make sure that it is valid
    gesture.trigger.type = evaluate_type(QString::fromStdString(gesture_type));
    if (gesture.trigger.type == TYPE_ERROR)
        return false;

    // Make sure we are using a valid finger count
    gesture.trigger.finger_count = finger_count;
    if (finger_count <= 0 || finger_count >= 5)
        return false;

    // Initialize the state of the gesture
    gesture.touch_count = 0;
    gesture.entered = false;
    gesture.fired = false;

    // Get the type of gesture, and its corresponding direction.
    switch (gesture.trigger.type) {
        case TYPE_SWIPE:
            gesture.trigger.direction = evaluate_direction(QString::fromStdString(gesture_direction));
            if (gesture.trigger.direction == DIRECTION_ERROR)
                return false;
            break;
        case TYPE_EDGE_SWIPE:
        case TYPE_TAP_AND_HOLD:
        case TYPE_DOUBLE_TAP:
        default:
            return false;
    }

    m_triggers.append(gesture);
    return true;
}
