//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef PV_VM_INPUT_FACTORY__H
#define PV_VM_INPUT_FACTORY__H

#include <QObject>
#include <input_server.h>
#include <pv_vm_input.h>
#include <vm_input_factory.h>

class pv_vm_input_factory_t : public vm_input_factory_t
{

    Q_OBJECT;

public:
    pv_vm_input_factory_t(window_manager_t &wm, input_server_t &server);
    virtual ~pv_vm_input_factory_t() = default;
    virtual std::shared_ptr<vm_input_t> make_vm_input(std::shared_ptr<vm_base_t> vm);

signals:
    void add_guest(std::shared_ptr<vm_input_t> vm_input);
};

#endif // PV_VM_INPUT_FACTORY__H
