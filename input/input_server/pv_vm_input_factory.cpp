//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <pv_vm_input_factory.h>

pv_vm_input_factory_t::pv_vm_input_factory_t(window_manager_t &wm, input_server_t &input_server) : vm_input_factory_t(wm)
{
    QObject::connect(this, &pv_vm_input_factory_t::add_guest, &input_server, &input_server_t::add_guest);
}

std::shared_ptr<vm_input_t>
pv_vm_input_factory_t::make_vm_input(std::shared_ptr<vm_base_t> vm)
{
    std::shared_ptr<vm_input_t> vm_input = std::make_shared<pv_vm_input_t>(vm, m_wm);

    emit add_guest(vm_input);

    return vm_input;
}
