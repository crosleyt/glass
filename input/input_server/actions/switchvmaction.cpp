//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <switchvmaction.h>

switch_vm_action_t::switch_vm_action_t(const int32_t slot) : m_slot(slot) {}
switch_vm_action_t::~switch_vm_action_t(void) {}

void
switch_vm_action_t::operator()()
{
    emit switch_vm(m_slot);
}
