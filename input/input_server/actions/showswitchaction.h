//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef SHOW_SWITCH_ACTION__H
#define SHOW_SWITCH_ACTION__H

#include <QStack>
#include <inputaction.h>
#include <vm.h>
#include <xt_input_global.h>

class show_switch_action_t : public input_action_t
{
    Q_OBJECT

public:
    show_switch_action_t();
    ~show_switch_action_t(void);

    void set_highlight_vm(uuid_t highlight);
    void operator()();

signals:

    void reveal_switcher();
    void highlighted_vm_changed(uuid_t highlight);
    void advance_vm_right(uuid_t highlighted_vm);
    void advance_vm_left(uuid_t highlighted_vm);
    void show_vm_switcher(void);

public slots:

    void advance_highlight_vm_right(void);
    void advance_highlight_vm_left(void);
    void reset_switcher(uuid_t vm);

private:
    uuid_t m_highlight_vm;
};

#endif //SHOW_SWITCH_ACTION__H
