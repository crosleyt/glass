//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <advanceswitcheraction.h>

advance_switcher_action_t::advance_switcher_action_t(void)
{
}

advance_switcher_action_t::~advance_switcher_action_t(void)
{
}

void
advance_switcher_action_t::operator()()
{
    emit advance_switcher();
}
