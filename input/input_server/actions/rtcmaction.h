//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef REVERT_TO_CLONED_MODE_ACTION__H
#define REVERT_TO_CLONED_MODE_ACTION__H

#include <inputaction.h>

class revert_to_cloned_mode_action_t : public input_action_t
{
    Q_OBJECT

public:
    revert_to_cloned_mode_action_t(void);
    ~revert_to_cloned_mode_action_t(void);

    void operator()();
};

#endif //REVERT_TO_CLONED_MODE_ACTION__H
