//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <vkbdsink.h>

//TODO: Should we make a define for the initial width and height?
// Also need to implement a tracing function for each method
vkbd_sink_t::vkbd_sink_t(int32_t domid, int32_t num_devices, bool is_linux_guest) : m_domid(domid),
                                                                                    m_num_devices(num_devices),
                                                                                    m_guest_width(32768),
                                                                                    m_guest_height(32768),
                                                                                    m_led_code(0)
{
    // Create a backend device for the total number of devices, and add each to the list of backend devices
    for (uint32_t i = 0; i < m_num_devices; i++) {
        std::shared_ptr<xen_vkbd_backend_t> vkbd_back = std::make_shared<xen_vkbd_backend_t>(m_domid, is_linux_guest);

        // If we are unable to obtain a pointer to a backend device, error out
        if (!vkbd_back) {
            qDebug() << "Failed to register vkbd backend for dom[" << QString(m_domid) << "]";
        }

        QObject::connect(vkbd_back.get(), &xen_vkbd_backend_t::connection_status, this, &vkbd_sink_t::backend_connection_status);

        m_absolute_enabled = vkbd_back->absolute_enabled();

        // Initialize the device, only adding it to the list if the initialization is successful
        if (false == vkbd_back->init()) {
            qDebug() << "Failed to initialize the vkbd backend";
        } else {
            m_backend_list.append(vkbd_back);
        }
    }
}

void
vkbd_sink_t::enqueue_input_event(xt_input_event event)
{
    union xenkbd_in_event in_event;

    memset(&in_event, 0x00, sizeof(in_event));

    vg_debug() << "vkbd_sink_t::enqueue_input_event";

    // Have to convert from xt_input_event to xenkbd_in_event
    // TODO: guest support is absolute XOR relative...
    // we just pass all events through, but should be converted according
    // to xen_vkbd_backend_t.m_use_absolute
    switch (event.type) {
        case XT_TYPE_KEY: {
            struct xenkbd_key *key_event = (struct xenkbd_key *) &in_event;

            if (event.flags) {
                uint32_t led_code = 0;
                switch (event.keyCode) {
                    case KEY_CAPSLOCK:
                        led_code = LED_CODE_CAPSLOCK;
                        break;
                    case KEY_NUMLOCK:
                        led_code = LED_CODE_NUMLOCK;
                        break;
                    case KEY_SCROLLLOCK:
                        led_code = LED_CODE_SCROLLLOCK;
                        break;
                }

                if (led_code != 0) {
                    if ((m_led_code & led_code) == led_code) {
                        m_led_code &= (~led_code);
                    } else {
                        m_led_code |= led_code;
                    }
                    // Update the led code
                    emit keyboard_led_changed(m_led_code);
                }
            }

            key_event->type = XENKBD_TYPE_KEY;
            key_event->pressed = event.flags;
            key_event->keycode = event.keyCode;

            write_event(&in_event);
            break;
        }
        case XT_TYPE_RELATIVE: {
            // Handle mouse clicks
            if (event.keyCode != 0) {
                struct xenkbd_key *key_event = (struct xenkbd_key *) &in_event;

                key_event->type = XENKBD_TYPE_KEY;
                key_event->pressed = event.flags;
                key_event->keycode = event.keyCode;
            } else {
                // Handle mouse moves
                struct xenkbd_motion *rel_event = (struct xenkbd_motion *) &in_event;

                rel_event->type = XENKBD_TYPE_MOTION;
                rel_event->rel_x = event.data.relative.relX;
                rel_event->rel_y = event.data.relative.relY;
                rel_event->rel_z = event.data.relative.relZ;
            }

            write_event(&in_event);
            break;
        }
        case XT_TYPE_ABSOLUTE: {
            // Handle mouse clicks
            if (event.keyCode != 0) {
                struct xenkbd_key *key_event = (struct xenkbd_key *) &in_event;

                key_event->type = XENKBD_TYPE_KEY;
                key_event->pressed = event.flags;
                key_event->keycode = event.keyCode;
            } else {
                struct xenkbd_position *abs_event = (struct xenkbd_position *) &in_event;
                abs_event->type = XENKBD_TYPE_POS;

                //Convert the provided event into the coordinate space accepted by the kbdfront instance running in the guest.
                auto scaled_point = this->scale_absolute_event(event);
                abs_event->abs_x = scaled_point.x();
                abs_event->abs_y = scaled_point.y();
                abs_event->rel_z = relZ_float_to_int(event.data.relative.relZ);
            }

            write_event(&in_event);
            break;
        }
        case XT_TYPE_TOUCH_DOWN: {
            struct xenkbd_mtouch *mt_event = (struct xenkbd_mtouch *) &in_event;
            mt_event->type = XENKBD_TYPE_MTOUCH;
            mt_event->event_type = XENKBD_MT_EV_DOWN;
            mt_event->contact_id = event.data.touch.slot;
            mt_event->u.pos.abs_x = event.data.touch.absX;
            mt_event->u.pos.abs_y = event.data.touch.absY;
            write_event(&in_event);
            break;
        }
        case XT_TYPE_TOUCH_UP: {
            struct xenkbd_mtouch *mt_event = (struct xenkbd_mtouch *) &in_event;
            mt_event->type = XENKBD_TYPE_MTOUCH;
            mt_event->event_type = XENKBD_MT_EV_UP;
            mt_event->contact_id = event.data.touch.slot;
            write_event(&in_event);
            break;
        }
        case XT_TYPE_TOUCH_MOVE: {
            struct xenkbd_mtouch *mt_event = (struct xenkbd_mtouch *) &in_event;
            mt_event->type = XENKBD_TYPE_MTOUCH;
            mt_event->event_type = XENKBD_MT_EV_MOTION;

            mt_event->contact_id = event.data.touch.slot;
            mt_event->u.pos.abs_x = event.data.touch.absX;
            mt_event->u.pos.abs_y = event.data.touch.absY;
            write_event(&in_event);
            break;
        }
        case XT_TYPE_TOUCH_FRAME: {
            struct xenkbd_mtouch *mt_event = (struct xenkbd_mtouch *) &in_event;
            mt_event->type = XENKBD_TYPE_MTOUCH;
            mt_event->event_type = XENKBD_MT_EV_SYN;
            mt_event->contact_id = event.data.touch.slot;
            write_event(&in_event);
            break;
        }
#ifdef XENKBD_TYPE_TTOOL
        case XT_TYPE_TOOL_PROXIMITY:
        case XT_TYPE_TOOL_TIP:
        case XT_TYPE_TOOL_AXIS:
        case XT_TYPE_TOOL_BUTTON: {
            struct xenkbd_ttool *tt_event = (struct xenkbd_ttool *) &in_event;
            tt_event->type = XENKBD_TYPE_TTOOL;
            tt_event->event_type = event.type - XT_TYPE_TOOL_PROXIMITY; /* Assumes both types are defined in the same incremental order. */
            tt_event->tool_type = event.data.tablet_tool.tool_type;
            tt_event->flags = event.data.tablet_tool.flags;
            tt_event->absX = event.data.tablet_tool.absX;
            tt_event->absY = event.data.tablet_tool.absY;
            tt_event->pressure = event.data.tablet_tool.pressure;
            tt_event->button = event.data.tablet_tool.button;
            write_event(&in_event);
            break;
        }
#endif
        default: {
            qDebug() << "Invalid event type, dropping (" << QString(event.type) << ").";
        } break;
    }
}

void
vkbd_sink_t::write_event(union xenkbd_in_event *event)
{
    //print kbd event to debug
    vg_debug() << "vkbd_sink_t::afo::write_event: " << get_kbd_event_string(event);

    // Write the event to each device in the backend list
    for (auto &backend : m_backend_list) {
        // May want to filter our whether we write relative and absolute events based on the sink we have.
        backend->write_to_ring(event);
    }
}

/**
 * Scales an absolute event to the coordinate space used by the
 * kbdfront instance in the guest domain.
 *
 * @param event The _absolute_ event that specifies the event to
 *              be scaled.
 *
 * @return The point to be transmitted to the guest, in the kbdfront
 *         coordinate space.
 */
point_t
vkbd_sink_t::scale_absolute_event(const xt_input_event &event)
{
    // Compute the x,y coordinate for the absolute event in the kbdfront reference frame.
    uint32_t target_x = (event.data.absolute.absX * m_guest_width) / (event.data.absolute.absXMax - 1);
    uint32_t target_y = (event.data.absolute.absY * m_guest_height) / (event.data.absolute.absYMax - 1);

    point_t target_point(target_x, target_y);

    return target_point;
}

void
vkbd_sink_t::backend_connection_status(bool is_connected)
{
    // Emit a signal if all backends have connected or disconnected.
    for (auto &backend : m_backend_list)
        if (backend->is_connected() != is_connected)
            return;

    m_connected = is_connected;
    emit connection_status(is_connected);
}

/**
 * @brief vkbd_sink_t::get_kbd_event_string
 *  For debugging purposes, present the event in string
 * form for logging to console or file
 * @return The event in string form.
 */
QString
vkbd_sink_t::get_kbd_event_string(union xenkbd_in_event *event)
{
    QString retValue = "PROBLEM";

    /*
    XENKBD_TYPE_MOTION
    XENKBD_TYPE_KEY
    XENKBD_TYPE_POS
    XENKBD_TYPE_MTOUCH

    Multi-touch event sub-codes

    XENKBD_MT_EV_DOWN
    XENKBD_MT_EV_UP
    XENKBD_MT_EV_MOTION
    XENKBD_MT_EV_SYN
    XENKBD_MT_EV_SHAPE
    XENKBD_MT_EV_ORIENT
    */

    switch (event->type) {

        case XENKBD_TYPE_MOTION:
            retValue = QString("%1 relX: %2 relY: %3 relZ: %4").arg("MOTION: ").arg(event->motion.rel_x).arg(event->motion.rel_y).arg(event->motion.rel_z);
            break;
        case XENKBD_TYPE_KEY:
            retValue = QString("%1 code: %2 isPressed: %3").arg("KEY: ").arg(event->key.keycode).arg(event->key.pressed);
            break;
        case XENKBD_TYPE_POS:
            retValue = QString("%1 absX: %2 absY %3 relZ %4").arg("POS: ").arg(event->pos.abs_x).arg(event->pos.abs_y).arg(event->pos.rel_z);
            break;
        case XENKBD_TYPE_MTOUCH:
            retValue = QString("%1 slot %2 ").arg("MTOUCH: ").arg(event->mtouch.contact_id);

            //need to switch on the subcode
            switch (event->mtouch.event_type) {
                case XENKBD_MT_EV_DOWN:
                    retValue += QString("%1 absX %2 absY %3").arg("EV_DOWN").arg(event->mtouch.u.pos.abs_x).arg(event->mtouch.u.pos.abs_y);
                    break;
                case XENKBD_MT_EV_UP:
                    retValue += QString("%1").arg("EV_UP");
                    break;
                case XENKBD_MT_EV_MOTION:
                    retValue += QString("%1 absX %2 absY %3").arg("EV_MOTION").arg(event->mtouch.u.pos.abs_x).arg(event->mtouch.u.pos.abs_y);
                    break;
                case XENKBD_MT_EV_SYN:
                    retValue += QString("%1").arg("SYN");
                    break;
                case XENKBD_MT_EV_SHAPE:
                    retValue += QString("%1 major: %2 minor: %3").arg("EV_SHAPE").arg(event->mtouch.u.shape.major).arg(event->mtouch.u.shape.minor);
                    break;
                case XENKBD_MT_EV_ORIENT:
                    retValue += QString("%1 angle: %2 ").arg("EV_ORIENT ").arg(event->mtouch.u.orientation);
                    break;
            }
            break;
#ifdef XENKBD_TYPE_TTOOL
        case XENKBD_TYPE_TTOOL: {
            struct xenkbd_ttool *ttool = (struct xenkbd_ttool *) &event->ttool;
            retValue = QString("%1 type: %2 absX: %3 absY: %4 flags: %5 pressure: %6 button: %7").arg("TOOL").arg(ttool->tool_type).arg(ttool->absX).arg(ttool->absY).arg(ttool->flags).arg(ttool->pressure).arg(ttool->button);
            break;
        }
#endif
        default:
            break;
    }

    return retValue;
}
