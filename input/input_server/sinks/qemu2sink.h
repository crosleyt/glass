//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef QEMU2_SINK__H
#define QEMU2_SINK__H

#include <glass_types.h>

#include <guestinputsink.h>
#include <ivc_channel.h>
#include <linux/input.h>
#include <vm_base.h>

#include <QMutex>
#include <QMutexLocker>

#define QEMU2_INPUT_PORT 1589

class qemu2_sink_t : public guest_input_sink_t
{
    Q_OBJECT
    using domid_t = int16_t;

public:
    explicit qemu2_sink_t(domid_t domid);
    virtual ~qemu2_sink_t();

    void handle_disconnect();

public slots:
    void new_client(void *c);
    void enqueue_input_event(xt_input_event event);

private:
    uint32_t m_domid;

    ivc_server_t m_ivc_server;
    std::unique_ptr<ivc_connection_t> m_input_channel;

    uint32_t m_led_code;

    QMutex m_lock;

    bool transform_xt_input_event_for_qemu(xt_input_event_t *new_event, qemu_xt_input_event *qemu_event);
};

#endif // QEMU_SINK__H
