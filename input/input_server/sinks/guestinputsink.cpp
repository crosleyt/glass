//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <guestinputsink.h>

guest_input_sink_t::guest_input_sink_t(void)
{
}

bool
guest_input_sink_t::is_stubdom(void)
{
    return true;
}

/* Since a sink may use an integer relZ, this helper method keeps track of the leftover after rounding. */
int32_t
guest_input_sink_t::relZ_float_to_int(float relZ)
{
    float relZ_accumulated = relZ + m_relZ_fractional;
    int32_t relZ_accumulated_int = relZ_accumulated;

    m_relZ_fractional = relZ ? relZ_accumulated - (float) relZ_accumulated_int : 0;
    return relZ_accumulated_int;
}
