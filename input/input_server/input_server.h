//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef INPUTSERVER_PRIVATE_H
#define INPUTSERVER_PRIVATE_H

#include <display.h>
//#include <hostinputfilter.h>
#include <QDebug>
#include <QMutex>
#include <QStack>
#include <QTimer>
#include <glass_types.h>
#include <guestinputsink.h>
#include <input.h>
#include <input_dbus_proxy.h>
#include <input_plane.h>
#include <inputconfigparser.h>
#include <inputserverfilter.h>
#include <vm.h>
#include <window_manager.h>
#include <xt_input_global.h>

#define INPUT_CONFIG_URL "db:///inputserver"
#define INPUT_CONFIG_DEFAULT_URL "file:///etc/vglass/inputserver_config"

class input_server_t : public input_t
{
    Q_OBJECT

public:
    explicit input_server_t(window_manager_t &wm);
    input_server_t(window_manager_t &wm, std::string input_config_path);
    virtual ~input_server_t(void);
    bool register_input_filter(const QString name, std::shared_ptr<input_server_filter_t> filter);
    void parse_config(json &config);
    virtual const std::shared_ptr<vm_input_t> get_focused_vm(void) const;
    json load_config();
    bool save_config(const json config);
    window_manager_t &get_wm();

public slots:

    /*
      These slots are exposed in the public API, so other plugins
      can connect to them.
    */

    // Host Displays
    virtual void update_input_guest(const std::shared_ptr<vm_t> &vm);
    virtual void set_shared_desktop_owner(const std::shared_ptr<vm_t> &vm);
    void add_guest(const std::shared_ptr<vm_input_t> &vm);
    void remove_guest(const std::shared_ptr<vm_t> &vm);
    void reboot_guest(const uuid_t uuid);
    void sleep_guest(const uuid_t uuid);
    void input_action(const std::shared_ptr<input_action_t> &input_action);

    /*
      These slots are for internal input server APIs, not to be accessed
      by other plugins
    */

    // Input events from host input filters enter the input server here
    void event_slot(xt_input_event event, std::shared_ptr<vm_input_t> target_vm = NULL);

    // Send an event directly to a guest, bypassing focus switching and filters.
    // This is both internally useful and useful for filter results!
    void event_to_guest(const std::shared_ptr<vm_input_t> &guest, xt_input_event event);

    // These functions control the displaying a graphical list of VMs with a
    // specific member of the list highlighted
    void show_switcher(void);
    void hide_switcher(uuid_t uuid);
    void advance_vm_right(uuid_t highlighted_vm);
    void advance_vm_left(uuid_t highlighted_vm);
    void set_focused_vm(void);
    void set_focused_vm(const std::shared_ptr<vm_input_t> &vm);
    bool switch_focus(const domid_t domid, const bool force);
    void set_highlight_vm(uuid_t vm);
    void switch_vm(const int32_t slot);
    void idle_timeout(const QString &idle_name);
    void focus_changed(const uuid_t uuid);

    bool get_seamless_mousing();
    bool set_seamless_mousing(const bool enabled);

    void wake_up_guest(uuid_t uuid);

    //used to handle brightness changes requested from keystrokes
    void update_power_source(uint using_ac);

    void increase_brightness();
    void decrease_brightness();
signals:

    void set_mouse_speed(float mouse_speed);
    void set_touchpad_tap_to_click(bool enabled);
    void set_touchpad_scrolling(bool enabled);
    void set_touchpad_speed(float touchpad_speed);
    void idle_timer_update(const QString &timerName, int32_t timeout);
    void reset_hotspot_signal(void);
    void save_screenshot(void);
    void show_vm_switcher(void);
    void hide_vm_switcher(void);
    void advance_switcher_right(uuid_t highlighted_vm);
    void advance_switcher_left(uuid_t highlighted_vm);
    void focused_vm_changed(const std::shared_ptr<vm_input_t> &vm);
    void focused_vm_changed_region(uuid_t uuid);
    void highlight_vm_changed(uuid_t uuid);
    void heartbeat();

    void center_mouse(uuid_t uuid);

private:
    bool is_pointer_event(xt_input_event event);

    // Check whether the input event causes an outside guest event,
    // like switching VMs, or we need a dom0 terminal
    bool filter_event(std::shared_ptr<vm_input_t> &vm, xt_input_event &event);

    window_manager_t &m_wm;
    std::shared_ptr<vm_input_t> m_focused_vm;
    uuid_t m_default_guest_uuid;
    std::shared_ptr<vm_input_t> m_default_guest;
    qhash_t<uuid_t, std::shared_ptr<vm_input_t>> m_guest_map;
    list_t<std::shared_ptr<input_source_t>> m_input_sources;
    list_t<std::shared_ptr<input_server_filter_t>> m_filter_list;
    uuid_t m_reboot_guest_uuid;
    input_plane_t m_input_plane;
    domid_t m_unhandled_switch;

    std::shared_ptr<input_dbus_proxy> m_input_dbus_proxy;

    //FIXME boolean for preserving the original multitouch implementation
    //remove when completel
    bool m_fakemultitouch;

    //used to handle brightness changes
    std::unique_ptr<DBus> m_dbus;
    std::unique_ptr<db_dbus_t> m_db;
    std::shared_ptr<QDBusConnection> m_dbus_connection;
    std::shared_ptr<xcpmd_dbus_t> m_xcpmd_dbus;

    QString power_source;
};

#endif // INPUTSERVER_PRIVATE_H
