#ifndef DBUS_H
#define DBUS_H

//
// DBus
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//

// ============================================================================
// Includes
// ============================================================================

#include <QDBusAbstractAdaptor>
#include <QDBusConnection>
#include <QtCore>
#include <memory> // std::shared_ptr, std::make_shared

// ============================================================================
// Definition
// ============================================================================

class DBus : public QObject
{
    Q_OBJECT

public:
    DBus();
    virtual ~DBus();

    QDBusConnection &systemBus();

    // DBUS Service Stuff
    template <class DBUSADAPTOR, class C>
    bool registerService(C *const obj, const QString &service_name)
    {
        // Check connection to the system bus
        if (!isConnected()) {
            return false;
        }

        // Setup our adaptor and register our service
        return registerService(std::make_shared<DBUSADAPTOR>(obj), service_name);
    };
    bool registerService(const std::shared_ptr<QDBusAbstractAdaptor> dbusAdaptor, const QString &service_name);
    bool unregisterService(const QString &service_name);
    bool registerObject(QObject *obj, const QString &object_name);
    bool unregisterObject(const QString &object_name);

private:
    bool isConnected() const;

private:
    QDBusConnection sysBus;
    QHash<QString, std::shared_ptr<QDBusAbstractAdaptor>> dbusAdaptors;
};

#endif // DBUS_H
