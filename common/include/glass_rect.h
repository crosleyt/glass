//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef GLASS_RECT__H
#define GLASS_RECT__H

#include <QRect>

class glass_rect_t : public QRect
{
public:
    glass_rect_t() : QRect() {}
    glass_rect_t(const QPoint &top_left, const QPoint &bottom_left) : QRect(top_left, bottom_left) {}
    glass_rect_t(const QPoint &top_left, const QSize &size) : QRect(top_left, size) {}
    glass_rect_t(int x, int y, int w, int h) : QRect(x, y, w, h) {}
    glass_rect_t(const QRect &rect) : QRect(rect) {}
    virtual ~glass_rect_t() = default;

    bool operator<(const glass_rect_t &r2) const
    {
        // We'll use simple distance from the origin, skipping
        // the sqrt because it is inconsequential for our coordinate
        // system.
        uint32_t d1 = x() * x() + y() * y();
        uint32_t d2 = r2.x() * r2.x() + r2.y() * r2.y();

        if (d1 < d2) {
            return true;
        } else {
            return false;
        }
    }
};

#endif // GLASS_MAP__H
