//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef OBSERVABLE__H
#define OBSERVABLE__H

#ifndef set_type
#include <list>
#define set_type std::list
#endif

#ifndef string_type
#include <functional>
#include <string>
typedef std::string string_type;
#endif

#ifdef DEBUG
#include <iostream>
#define LOG(stakeholder, value)                                                              \
    do {                                                                                     \
        std::cout << " name[" << stakeholder->name() << "]::[" << value << "]" << std::endl; \
    } while (0)
#else
#define LOG(stakeholder, value)
#endif

class b_reporter
{
public:
    b_reporter(string_type name) : m_name(name) {}
    virtual ~b_reporter() {}

    const string_type name(void) const { return m_name; }

    bool operator==(const b_reporter &rhs) const
    {
        return (this->name() == rhs.name());
    }

    bool operator!=(const b_reporter &rhs) const
    {
        return (this->name() != rhs.name());
    }

protected:
    const string_type m_name;
};

class b_subscriber
{
public:
    b_subscriber(string_type name) : m_name(name) {}
    virtual ~b_subscriber() {}

    const string_type name(void) const { return m_name; }

protected:
    const string_type m_name;
};

class b_config_datum
{
public:
    b_config_datum(string_type name) : m_name(name) {}
    b_config_datum() : m_name("") {}
    virtual ~b_config_datum() {}

    const string_type &name(void) { return m_name; }

protected:
    string_type m_name;
};

template <typename T>
class subscriber : public b_subscriber
{
public:
    subscriber(string_type name, std::function<void(const T &)> notify) : m_notify(notify), b_subscriber(name)
    {
    }

    ~subscriber() = default;

    virtual void notify(const T &value) const
    {
        m_notify(value);
    }

private:
    std::function<void(const T &)> m_notify;
    string_type m_name;
};

template <typename T>
class config_datum;

template <typename T>
class reporter : public b_reporter
{
public:
    reporter(config_datum<T> &datum, string_type name, std::function<void(const T &)> update_backing) : m_update_backing(update_backing), m_config_datum(datum), b_reporter(name)
    {
        m_config_datum.register_reporter(*this);
    }

    ~reporter() { m_config_datum.unregister_reporter(*this); }

    void set(const T &value)
    {
        m_config_datum.set(*this, value);
    }

    virtual void update_backing(const T &value) const
    {
        m_update_backing(value);
    }

protected:
    std::function<void(const T &)> m_update_backing;

    // This shouldn't be set by anything other
    // than the config_datum itself, when register_reporter
    // is called (I don't particularly like this part,
    // I think there must be a better way).
    config_datum<T> &m_config_datum;

    // For logging purposes
    string_type m_name;
};

template <typename T>
class config_datum : public b_config_datum
{
    friend class reporter<T>;

public:
    config_datum(string_type name) : b_config_datum(name) {}
    ~config_datum() {}

    void register_subscriber(subscriber<T> &s)
    {
        m_subscribers.push_back(s);
        return;
    }

    void unregister_subscriber(const subscriber<T> &s)
    {
        m_subscribers.remove(s);
    }

    void register_reporter(reporter<T> &r)
    {
        for (const b_reporter &reporter : m_reporters) {
            // Might want to throw here.
            if (reporter == r)
                return;
        }

        m_reporters.push_back(r);

        return;
    }

    void unregister_reporter(const reporter<T> &r)
    {
        m_reporters.remove(r);
    }

    void set(const reporter<T> &r, const T &value) const
    {
        for (const reporter<T> &rep : m_reporters) {
            if (r != rep) {
                rep.update_backing(value);
            }
        }

        for (const subscriber<T> &sub : m_subscribers) {
            sub.notify(value);
        }
    }

private:
    set_type<subscriber<T>> m_subscribers;
    set_type<reporter<T>> m_reporters;
};

#endif // OBSERVABLE__H
