//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <iostream>
#include <plane.h>

int
main(int argc, const char **argv)
{
    (void) argc;
    (void) argv;

    /* I don't think we are using this?
    int banner = 25;
    int dw = 1280, dh = 1024;
    point_t rsp_xy(799, 599);

    plane_t input_plane(rect_t(0, 0, (1280 + 800), 1024), point_t(0, 0));
    plane_t display_plane(rect_t(0, 0, dw, dh), point_t(800, 0));
    plane_t render_plane(rect_t(0, 0, dw, dh - banner), point_t(0, banner));
    plane_t render_target_plane(rect_t(0, 0, 800, 600), point_t(50, 50));
    plane_t render_source_plane(rect_t(0, 0, 640, 480), point_t(0, 0));

    transform_t render_source_to_render_target = render_target_plane.from(render_source_plane);
    transform_t render_target_plane_to_render_plane = render_plane.translate(render_target_plane);
    transform_t render_plane_to_display_plane = display_plane.translate(render_plane);
    transform_t display_plane_to_input_plane = input_plane.translate(display_plane);

    transform_t render_source_to_input_plane;
    render_source_to_input_plane *= render_source_to_render_target;
    render_source_to_input_plane *= render_target_plane_to_render_plane;
    render_source_to_input_plane *= render_plane_to_display_plane;
    render_source_to_input_plane *= display_plane_to_input_plane;

    qDebug() << render_source_to_input_plane.map(point_t(0, 0));
    qDebug() << render_source_to_input_plane.map(point_t(639, 479));
    */

    return 0;
}
