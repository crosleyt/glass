//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include <render_target_plane.h>

render_target_plane_t::render_target_plane_t(rect_t rect,
                                             point_t plane_origin,
                                             std::shared_ptr<plane_t> parent) : plane_t(rect, plane_origin, parent),
                                                                                m_pv_render_source(nullptr),
                                                                                m_qemu_render_source(nullptr),
                                                                                m_edid_hash(0),
                                                                                m_key(0),
                                                                                m_use_pv(false)
{
    vg_debug() << "render target plane created: " << this;
}

render_target_plane_t::~render_target_plane_t()
{
    vg_debug() << "render target plane destroyed: " << this;

    if (m_pv_render_source) {
        m_pv_render_source = nullptr;
    }
}

void
render_target_plane_t::reset()
{
    m_qemu_render_source = nullptr;
    m_pv_render_source = nullptr;
}

transform_t
render_target_plane_t::from(plane_t &plane)
{
    if (!this->render_source()) {
        return transform_t();
    }

    return scale(plane);
}

transform_t
render_target_plane_t::to(plane_t &plane)
{
    (void) plane;
    if (!this->render_source()) {
        return transform_t();
    }

    return scale(plane).inverted();
}

transform_t
render_target_plane_t::translate(plane_t &plane)
{
    (void) plane;
    return transform_t::fromTranslate(this->origin().x(), this->origin().y());
}

transform_t
render_target_plane_t::scale(plane_t &plane)
{
    (void) plane;
    qreal sx = (qreal) this->rect().width() / (qreal) this->render_source()->rect().width();
    qreal sy = (qreal) this->rect().height() / (qreal) this->render_source()->rect().height();
    return transform_t::fromScale(sx, sy);
}

void
render_target_plane_t::set_key(window_key_t key)
{
    m_key = key;
}

window_key_t
render_target_plane_t::key()
{
    return m_key;
}

void
render_target_plane_t::set_origin(const point_t &origin)
{
    m_plane_origin = origin;
    m_updated = true;
}

void
render_target_plane_t::force_qemu_render_source(bool qemu)
{
    if (qemu && m_qemu_render_source) {
        m_use_pv = false;
    } else if (!qemu && m_pv_render_source) {
        m_use_pv = true;
    }
}

void
render_target_plane_t::attach_render_source(std::shared_ptr<render_source_plane_t> render_source, bool qemu)
{
    if (qemu) {
        m_qemu_render_source = render_source;
    } else {
        m_pv_render_source = render_source;
    }
}

render_source_plane_t *
render_target_plane_t::render_source()
{
    if (m_use_pv) {
        return m_pv_render_source.get();
    } else {
        return m_qemu_render_source.get();
    }
}

render_source_plane_t *
render_target_plane_t::pv_render_source()
{
    return m_pv_render_source.get();
}

render_source_plane_t *
render_target_plane_t::qemu_render_source()
{
    return m_qemu_render_source.get();
}
