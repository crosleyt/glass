//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef DESKTOP_PLANE__H
#define DESKTOP_PLANE__H

#include <QObject>
#include <display_plane.h>

// See definitions in common/include/plane.h

class desktop_plane_t : public QObject, public plane_t
{
    Q_OBJECT;

public:
    desktop_plane_t(uuid_t uuid = uuid_t(),
                    rect_t rect = rect_t(0, 0, 0, 0),
                    point_t plane_origin = QPoint(0, 0),
                    bool renderable = true,
                    int mode = UNSPECIFIED,
                    std::shared_ptr<plane_t> parent = nullptr);
    virtual ~desktop_plane_t();

    enum {
        UNSPECIFIED,
        CLONED,
        SHARED,
        PINNED,
    };
    int mode() { return m_mode; }

    virtual void add_display(std::shared_ptr<display_plane_t> display);
    virtual void add_render_target(uuid_t uuid, std::shared_ptr<render_target_plane_t> render_target);

    virtual display_plane_t *display(point_t point);
    virtual display_plane_t *display(uint32_t key);
    virtual render_target_plane_t *render_target(uuid_t uuid, point_t point);
    virtual render_target_plane_t *render_target(uuid_t uuid, display_plane_t *display);
    virtual render_target_plane_t *render_target(uuid_t uuid, window_key_t key);
    virtual render_source_plane_t *render_source(uuid_t uuid, window_key_t key);

    virtual list_t<std::shared_ptr<display_plane_t>> &displays();
    virtual list_t<std::shared_ptr<render_target_plane_t>> &render_targets(uuid_t uuid);
    virtual hash_t<uuid_t, list_t<std::shared_ptr<render_target_plane_t>>> &render_targets();
    virtual void remove_render_target(uuid_t uuid, render_target_plane_t *render_target);
    virtual bool renderable();

    virtual region_t &visible_region();
    virtual void remove_guest(uuid_t uuid);
    display_plane_t *current_display() { return m_current_display; }
    void set_current_display(display_plane_t *d) { m_current_display = d; }

    QMutex *lock() { return &m_lock; }

    desktop_uuid_t uuid() { return m_uuid; }

    // This fixes m_plane and m_visible_region to be at (0,0) after all displays have been added
    virtual void reorigin_displays();
    virtual void translate_planes();

    virtual void set_qemu_source(uuid_t uuid, std::shared_ptr<render_source_plane_t> qemu);
    virtual void attach_qemu_source(uuid_t uuid);

    void reset();

public slots:
    void reset_render_planes(uuid_t uuid);

private:
    void dump_render_targets(uuid_t uuid);

    // Host side resources
    list_t<std::shared_ptr<display_plane_t>> m_displays;

    // Guest side resources
    hash_t<uuid_t, list_t<std::shared_ptr<render_target_plane_t>>> m_render_targets;

    bool m_renderable;

    region_t m_visible_region;

    // To be used for input_clamping
    display_plane_t *m_current_display = nullptr;

    QMutex m_lock;

    desktop_uuid_t m_uuid;
    hash_t<uuid_t, std::shared_ptr<render_source_plane_t>> m_qemu_source;

    int m_mode = UNSPECIFIED;
};

#endif // DESKTOP_PLANE__H
