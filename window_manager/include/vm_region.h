//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef VM_REGION__H
#define VM_REGION__H

#include <glass_types.h>

#include <QObject>

#include <banner_overlay.h>
#include <desktop_plane.h>
#include <overlay.h>
#include <render_plane.h>
#include <render_target_plane.h>
#include <vm_base.h>

class vm_region_t : public QObject
{
    Q_OBJECT;

public:
    vm_region_t(std::shared_ptr<vm_base_t> base) : m_base(base)
    {
        m_visible_update = true;
        m_dirty_update = true;
        m_grabbed = false;
    }

    virtual ~vm_region_t() = default;

    virtual uuid_t uuid()
    {
        if (m_base) {
            return m_base->uuid();
        } else {
            return uuid_t();
        }
    }

    virtual std::shared_ptr<vm_base_t> base()
    {
        return m_base;
    }

    virtual const region_t &dirty_region()
    {
        return m_dirty_region;
    }

    virtual const region_t &visible_region()
    {
        return m_visible_region;
    }

    virtual void process_updates() = 0;
    virtual void process_extra_vm_input(point_t point, bool mouse_down) = 0;

    virtual bool seamless() { return 0; }
    virtual bool grabbed() { return m_grabbed; }
    virtual point_t origin(window_key_t key)
    {
        (void) key;
        return point_t();
    }

    virtual region_t create_visible_region(desktop_plane_t *desktop,
                                           display_plane_t *display_plane,
                                           render_target_plane_t *rtp);

    virtual bool visible_updated() { return m_visible_update; }
    virtual void set_visible_updated(bool update) { m_visible_update = update; }

    virtual void clear_visible_region()
    {
        m_last_visible_region = m_visible_region;
        m_visible_region = region_t();
    }

    virtual region_t create_dirty_region(desktop_plane_t *desktop,
                                         display_plane_t *display_plane,
                                         render_target_plane_t *rtp);

    virtual bool dirty_updated() { return m_dirty_update; }
    virtual void clear_dirty_region()
    {
        m_dirty_update = false;
        m_dirty_region = region_t();
    }

    virtual void render_overlays(QPainter &painter, desktop_plane_t *desktop, display_plane_t *display_plane, render_target_plane_t *rtp, region_t &display_clip, region_t &painted_clip);

    virtual region_t &guest_desktop()
    {
        return m_guest_desktop;
    }

public slots:

    virtual void restore_qemu()
    {
    }

    virtual void add_dirty_rect(rect_t rect)
    {
        m_dirty_update = true;
        m_dirty_region += rect;
    }

    virtual void add_dirty_region(region_t region)
    {
        m_dirty_update = true;
        m_dirty_region += region;
    }

    virtual void add_visible_rect(rect_t rect)
    {
        m_visible_update = true;
        m_visible_region += rect;
    }

    virtual void add_visible_region(region_t region)
    {
        m_visible_update = true;
        m_visible_region += region;
    }

    virtual void clip_visible_region(region_t region)
    {
        m_visible_region &= region;
        for (auto &overlay : m_overlays) {
            if (overlay) {
                overlay->clip_visible_region(m_visible_region);
            }
        }
    }

    virtual void clip_dirty_region(region_t region)
    {
        m_dirty_region &= region;
        for (auto &overlay : m_overlays) {
            if (overlay) {
                overlay->clip_visible_region(m_dirty_region);
            }
        }
    }

    virtual list_t<std::shared_ptr<overlay_t>> &overlays()
    {
        return m_overlays;
    }

signals:
    void update_render_targets(desktop_plane_t *desktop, uuid_t uuid);
    void update_render_source(uuid_t uuid, uint32_t key, rect_t rect);
    void calculate_guest_size(uuid_t uuid);

protected:
    std::shared_ptr<vm_base_t> m_base;

    region_t m_dirty_region;
    bool m_dirty_update;

    region_t m_visible_region;
    region_t m_last_visible_region;
    bool m_visible_update;
    list_t<std::shared_ptr<overlay_t>> m_overlays;

    bool m_grabbed;

    std::shared_ptr<banner_overlay_t> m_banner;

    region_t m_guest_desktop;
};

#endif // VM_REGION__H
