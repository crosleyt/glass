//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef BATTERY_OVERLAY_H
#define BATTERY_OVERLAY_H

#include <overlay.h>

class battery_overlay_t : public overlay_t
{
public:
    battery_overlay_t(uint32_t display_id, region_t region);
    virtual ~battery_overlay_t() = default;

    void set_value(uint32_t value)
    {
        m_value = value;
    }

    void set_power(bool power) { m_power = power; }

    void process_updates(std::shared_ptr<framebuffer_t> display);
    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display,
                region_t &display_clip,
                region_t &painted_clip);

private:
    uint32_t m_value;
    bool m_power;
    QImage m_battery_icon;

    rect_t m_icon_rect;
    rect_t m_icon_offset;
    rect_t m_value_rect;
};

#endif
