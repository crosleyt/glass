//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef BANNER_OVERLAY__H
#define BANNER_OVERLAY__H

#include <QString>

#include <glass_types.h>

#include <battery_overlay.h>
#include <overlay.h>
#include <vm_base.h>

class vm_region_t;

class banner_overlay_t : public overlay_t
{
public:
    banner_overlay_t(uint32_t display_id, point_t position, region_t region);
    virtual ~banner_overlay_t() = default;

    int height() { return m_height; }

    virtual void set_updated(bool update);

    void set_vm(std::shared_ptr<vm_base_t> vm);

    std::shared_ptr<battery_overlay_t> &battery_overlay();
    void set_battery_overlay(std::shared_ptr<battery_overlay_t> &battery_overlay);
    bool configured();

    void process_updates(std::shared_ptr<framebuffer_t> display);
    virtual void render(QPainter &p,
                        desktop_plane_t *desktop,
                        display_plane_t *display,
                        region_t &display_clip,
                        region_t &painted_private);

private:
    void render_image(int width);
    QFont get_max_font_size(const QString &text, const QString &fontFamily, int32_t height);

    uuid_t m_uuid;

    uint32_t m_update_count;
    int m_height;
    int m_width;

    point_t m_position;

    QString m_name;
    QString m_short_form;
    QString m_long_form;
    QString m_class_text;

    rect_t m_name_text_rect;
    rect_t m_class_text_rect;

    QFont m_font;

    QColor m_primary_domain_color;
    QColor m_secondary_domain_color;
    QColor m_text_color;

    bool m_configured;

    std::shared_ptr<battery_overlay_t> m_battery_overlay;
};

#endif // BANNER_OVERLAY__H
