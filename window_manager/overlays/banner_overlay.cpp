//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "banner_overlay.h"
#include <QTime>
#include <vm_region.h>

extern int32_t g_banner_height;

banner_overlay_t::banner_overlay_t(uint32_t display_id, point_t position, region_t region) : overlay_t(display_id, region),
                                                                                             m_update_count(6),
                                                                                             m_height(g_banner_height),
                                                                                             m_position(position)

{
    set_vm(nullptr);
    m_font = get_max_font_size(m_name, "DejaVu Sans", m_height);
    vg_info() << DTRACE << this;
}

QFont
banner_overlay_t::get_max_font_size(const QString &text, const QString &fontFamily, int32_t height)
{
    int32_t min = 0;
    int32_t max = 50;
    float padding_ratio = 0.06 * 2;

    // Nothing below this depends on specific values above

    // Only here in case min & max above get reversed
    if (min > max) {
        max ^= min ^= max ^= min;
    }

    int padding = height * padding_ratio;
    max = height - padding;
    // enforce our bounds on this
    height = std::min(max, std::max(min, height));

    uint32_t size = min + (max - min) / 2.;

    QFont font(fontFamily);
    font.setPixelSize(size);

    while (max - min > 1) {
        QFontMetrics metrics(font);
        rect_t bb = metrics.boundingRect(text);
        if (bb.height() == (height - padding)) {
            break;
        } else if (bb.height() > (height - padding)) {
            max = size - 1;
            size = min + (max - min) / 2.;
            font.setPixelSize(size);
        } else {
            min = size + 1;
            size = min + (max - min) / 2.;
            font.setPixelSize(size);
        }
    }

    return font;
}

void
banner_overlay_t::set_updated(bool update)
{
    m_updated = update;
    if (m_battery_overlay) {
        m_battery_overlay->set_updated(update);
    }
}

void
banner_overlay_t::set_vm(std::shared_ptr<vm_base_t> vm)
{
    if (!vm.get()) {
        m_configured = false;
        m_name = QString();
        m_short_form = QString();
        m_long_form = QString("UNCONFIGURED");
        m_primary_domain_color = Qt::white;
        m_secondary_domain_color = Qt::white;
        m_text_color = Qt::black;
    } else {
        m_configured = true;
        m_name = QString::fromStdString(vm->name());
        m_short_form = QString::fromStdString(vm->short_form());
        m_long_form = QString::fromStdString(vm->long_form());
        m_primary_domain_color = QString::fromStdString(vm->primary_domain_color());
        m_secondary_domain_color = QString::fromStdString(vm->secondary_domain_color());
        m_text_color = QString::fromStdString(vm->text_color());
    }
}

std::shared_ptr<battery_overlay_t> &
banner_overlay_t::battery_overlay()
{
    return m_battery_overlay;
}

void
banner_overlay_t::set_battery_overlay(std::shared_ptr<battery_overlay_t> &battery_overlay)
{
    m_battery_overlay = battery_overlay;
}

bool
banner_overlay_t::configured()
{
    return m_configured;
}

void
banner_overlay_t::process_updates(std::shared_ptr<framebuffer_t> display)
{
    if (!m_updated || !m_visible) {
        return;
    }

    QFontMetrics fm(m_font);
    int between_hashes_width;
    int before_hashes_width;
    bool still_too_long = false;
    auto first_hashes = display->width() / 5;
    auto second_hashes = display->width() - first_hashes;

    m_visible_region = rect_t(0, 0, display->width(), m_height);

    if (m_long_form.length() == 0) {
        m_long_form = QString("UNCONFIGURED");
    }

    m_class_text = m_long_form;
    between_hashes_width = (second_hashes - 156) - (first_hashes + 156);
    if (fm.horizontalAdvance(m_class_text) > between_hashes_width) {
        m_class_text = m_short_form;
    }

    while (fm.horizontalAdvance(m_class_text) > between_hashes_width) {
        m_class_text.chop(1);
        still_too_long = true;
    }

    if (still_too_long) {
        m_class_text.chop(3);
        m_class_text.append("...");
    }

    before_hashes_width = first_hashes - 10;
    still_too_long = false;

    while (fm.horizontalAdvance(m_name) > before_hashes_width) {
        m_name.chop(1);
        still_too_long = true;
    }

    if (still_too_long) {
        m_name.chop(3);
        m_name.append("...");
    }

    m_name_text_rect = rect_t(10, 0, fm.horizontalAdvance(m_name), m_height);
    m_class_text_rect = rect_t((display->width() / 2) - (fm.horizontalAdvance(m_class_text) / 2), 0, fm.horizontalAdvance(m_class_text), m_height);

    if (m_battery_overlay) {
        m_battery_overlay->process_updates(display);
    }
}

void
banner_overlay_t::render(QPainter &painter,
                         desktop_plane_t *desktop,
                         display_plane_t *display,
                         region_t &display_clip,
                         region_t &painted_clip)
{
    (void) desktop;
    (void) display;
    QFontMetrics fm(m_font);
    rect_t banner_rect;
    int width;

    if (!m_updated) {
        display_clip -= m_visible_region;
        painted_clip += m_visible_region;
        display->current_clip() += m_visible_region;
        return;
    }

    m_updated = false;

    banner_rect = rect_t(0, 0, painter.device()->width(), m_height);
    width = banner_rect.width();

    painter.setClipRegion(m_visible_region);
    painter.setClipping(true);
    painter.fillRect(banner_rect, m_primary_domain_color);

    QPen pen;
    auto first_hashes = width / 5;
    //24 * 6 + 12 = 156 (size of the hash marks)
    auto second_hashes = width - first_hashes;
    point_t start(first_hashes, 0);
    point_t finish(first_hashes + 12, banner_rect.height() - 1);

    pen = QPen(m_secondary_domain_color);

    for (int i = 0; i < 6; i++) {
        pen.setWidth(8);
        painter.setPen(pen);
        painter.drawLine(start, finish);
        start += point_t(24, 0);
        finish += point_t(24, 0);
    }

    start = point_t(second_hashes, 0);
    finish = point_t(second_hashes - 12, banner_rect.height() - 1);

    for (int i = 0; i < 6; i++) {
        pen.setWidth(8);
        painter.setPen(pen);
        painter.drawLine(start, finish);
        start -= point_t(24, 0);
        finish -= point_t(24, 0);
    }

    pen = QPen(m_text_color);
    painter.setPen(pen);
    painter.setFont(m_font);
    if (m_name.toLower() != m_class_text.toLower()) {
        painter.drawText(m_class_text_rect, Qt::AlignVCenter, m_class_text);
        painter.drawText(m_name_text_rect, Qt::AlignVCenter, m_name);
    } else {
        painter.drawText(m_class_text_rect, Qt::AlignVCenter, m_class_text);
    }

    point_t edge_start = point_t(0, m_height - 1);
    point_t edge_finish = point_t(painter.device()->width() - 1, m_height - 1);
    painter.setPen(Qt::black);
    painter.drawLine(edge_start, edge_finish);

    if (m_battery_overlay) {
        m_battery_overlay->render(painter, desktop, display, display_clip, painted_clip);
    }

    display_clip -= m_visible_region;
    painted_clip += m_visible_region;
    display->current_clip() += m_visible_region;
}
