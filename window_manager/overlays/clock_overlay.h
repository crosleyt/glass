//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef CLOCK_OVERLAY__H
#define CLOCK_OVERLAY__H

#include <overlay.h>

class clock_overlay_t : public overlay_t
{
public:
    clock_overlay_t(point_t position, region_t region);
    virtual ~clock_overlay_t() = default;

private:
    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display,
                region_t &display_clip,
                region_t &painted_clip);

    QHash<QPaintDevice *, QString> m_last_time;
    point_t m_position;
    uint32_t m_width;
    uint32_t m_height;
    rect_t m_clock_rect;
};

#endif // CLOCK_OVERLAY__H
