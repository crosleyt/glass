//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#include "window_border_overlay.h"
#include <QTime>

// border width
#define bw 5

window_border_overlay_t::window_border_overlay_t(vm_base_t &vm, point_t position, rect_t rect) : overlay_t(region_t(rect)),
                                                                                                 m_position(position),
                                                                                                 m_primary_domain_color(QString::fromStdString(vm.primary_domain_color())),
                                                                                                 m_secondary_domain_color(QString::fromStdString(vm.secondary_domain_color()))
{
    m_region &= region_t();
    set_border_rect(rect);
}

void
window_border_overlay_t::set_border_rect(rect_t rect)
{

    // Note to self: playing with regions just to shake the box
    // Regions are used in seamless overlay rendering
    region_t local_region;
    set_visible(true);

    // Top border
    m_top_border = rect_t(rect.x(), rect.y() - bw, rect.width(), bw);
    // left border
    m_left_border = rect_t(rect.x() - bw, rect.y() - bw, bw, bw + rect.height() + bw);
    // right border
    m_right_border = rect_t(rect.x() + rect.width(), rect.y() - bw, bw, bw + rect.height() + bw);
    // bottom border
    m_bottom_border = rect_t(rect.x(), rect.y() + rect.height(), rect.width(), bw);

    local_region += m_top_border;
    local_region += m_left_border;
    local_region += m_right_border;
    local_region += m_bottom_border;

    m_position = local_region.boundingRect().topLeft();
    m_region = m_visible_region = local_region;
}

void
window_border_overlay_t::render(QPainter &painter,
                                desktop_plane_t *desktop,
                                display_plane_t *display,
                                region_t &display_clip,
                                region_t &painted_clip)
{
    (void) display_clip;
    (void) painted_clip;

    render(painter, desktop, display);
}

void
window_border_overlay_t::render(QPainter &painter,
                                desktop_plane_t *desktop,
                                display_plane_t *display)
{
    painter.fillRect(desktop->map_to(display, m_top_border), QBrush(m_primary_domain_color));
    painter.fillRect(desktop->map_to(display, m_top_border), QBrush(m_secondary_domain_color, Qt::BDiagPattern));

    painter.fillRect(desktop->map_to(display, m_left_border), QBrush(m_primary_domain_color));
    painter.fillRect(desktop->map_to(display, m_left_border), QBrush(m_secondary_domain_color, Qt::BDiagPattern));

    painter.fillRect(desktop->map_to(display, m_right_border), QBrush(m_primary_domain_color));
    painter.fillRect(desktop->map_to(display, m_right_border), QBrush(m_secondary_domain_color, Qt::BDiagPattern));

    painter.fillRect(desktop->map_to(display, m_bottom_border), QBrush(m_primary_domain_color));
    painter.fillRect(desktop->map_to(display, m_bottom_border), QBrush(m_secondary_domain_color, Qt::BDiagPattern));
}
