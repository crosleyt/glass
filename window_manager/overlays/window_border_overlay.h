//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef WINDOW_BORDER_OVERLAY__H
#define WINDOW_BORDER_OVERLAY__H

#include <QString>

#include <glass_types.h>

#include <overlay.h>
#include <vm_base.h>

class window_border_overlay_t : public overlay_t
{
public:
    window_border_overlay_t(vm_base_t &vm, point_t position, rect_t rect);
    virtual ~window_border_overlay_t() = default;

    void set_border_rect(rect_t rect);

private:
    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display,
                region_t &display_clip,
                region_t &painted_clip);

    void render(QPainter &p,
                desktop_plane_t *desktop,
                display_plane_t *display);

    std::shared_ptr<QImage> m_image{nullptr};
    rect_t m_rect;
    point_t m_position;
    rect_t m_top_border;
    rect_t m_left_border;
    rect_t m_right_border;
    rect_t m_bottom_border;

    QColor m_primary_domain_color;
    QColor m_secondary_domain_color;
    QColor m_text_color;

    rect_t m_long_form_text_box;
    rect_t m_short_form_text_box;
    rect_t m_name_text_box;
    point_t m_rtp;

    region_t m_clip;

    bool m_seamless_update = false;
};

#endif // WINDOW_BORDER_OVERLAY__H
