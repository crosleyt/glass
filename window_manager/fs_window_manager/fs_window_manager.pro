QT       += core
include(../../common_include.prx)

TARGET = fs_wm
TEMPLATE = lib
DESTDIR = $$VG_BASE_DIR/lib

CONFIG += c++14

SOURCES += fs_window_manager.cpp
SOURCES += fs_vm_region_factory.cpp
SOURCES += ../common/window_manager.cpp
SOURCES += ../common/input_plane.cpp
SOURCES += ../common/desktop_plane.cpp
SOURCES += ../common/display_plane.cpp
SOURCES += ../common/render_source_plane.cpp
SOURCES += ../common/render_plane.cpp
SOURCES += ../common/render_target_plane.cpp
SOURCES += ../common/plane.cpp

SOURCES += ../overlays/banner_overlay.cpp
SOURCES += ../overlays/switcher_overlay.cpp
SOURCES += ../overlays/text_overlay.cpp
SOURCES += ../overlays/battery_overlay.cpp
SOURCES += ../common/vm_region.cpp

HEADERS += ../include/vm_region.h
HEADERS += ../include/window_manager.h
HEADERS += ../include/input_plane.h
HEADERS += ../include/desktop_plane.h
HEADERS += ../include/display_plane.h
HEADERS += ../include/render_source_plane.h
HEADERS += ../include/render_plane.h
HEADERS += ../include/render_target_plane.h
HEADERS += ../include/overlay.h
HEADERS += ../overlays/banner_overlay.h
HEADERS += ../overlays/switcher_overlay.h
HEADERS += ../overlays/text_overlay.h
HEADERS += ../overlays/battery_overlay.h

HEADERS += fs_window_manager.h
HEADERS += fs_vm_region.h
HEADERS += fs_vm_region_factory.h

target.path = /usr/lib
INSTALLS += target
