//
// Glass Display
//
// Copyright (C) 2016 - 2017 Assured Information Security, Inc. All rights reserved.
//
#ifndef FS_VM_REGION_FACTORY__H
#define FS_VM_REGION_FACTORY__H

#include <fs_vm_region.h>
#include <vm_region_factory.h>

class fs_vm_region_factory_t : public QObject, public vm_region_factory_t
{
    Q_OBJECT
public:
    explicit fs_vm_region_factory_t(window_manager_t &wm);

    virtual ~fs_vm_region_factory_t() = default;
    virtual std::shared_ptr<vm_region_t> make_vm_region(std::shared_ptr<vm_base_t> &vm);

signals:
    void vm_region_added(std::shared_ptr<vm_region_t> vm_region);
};

#endif // FS_VM_REGION_FACTORY__H
