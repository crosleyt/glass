#! /bin/sh
#
# Copyright (c) 2015-2019 Assured Information Security, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

### BEGIN INIT INFO
# Provides:             vglass
# Required-Start:       dbus-1 dbd network-daemon xenmgr
# Required-Stop:
# Default-Start:        5
# Default-Stop:         0 1 6
# Short-Description:    Starts glass
### END INIT INFO

DESC="vGlass"
EXEC="/usr/bin/glass"
PIDFILE="/var/run/glass.pid"
OPTS="-platform offscreen"

set -e

function start {
    local OPTS_2=""

    if [ $# -ne 1 ]; then
        OPTS_2="${@:2}"
    fi

    # If the user has booted by selecting the "console access"
    # configuration entry on their grub configuration...
    if grep -q "no-graphics" /proc/cmdline; then
        echo "Booted in console access mode. "

        # ... only start VGlass if this script was invoked
        # with the --force-start option.
        if [[ $* != *"--force-start"* ]]; then
            echo "Not starting VGlass. You can use --force-start to override this check."
            echo
            echo "(Note that OpenXT's troubleshooting options also disable VM autostart. You may want"
            echo " to start any NDVMs and/or UIVM before starting VGlass.)"
            exit 2
        fi
    fi

    # If the user selected the "clear disman configuration" boot option,
    # we'll clear the disman database keys prior to starting.
    if grep -q "reset-disman" /proc/cmdline; then
        echo "Boot argument set; resetting the disman configuration.\n"
        /usr/bin/db-rm /disman
    fi

    # Wait until XenMgr has come up.
    xec > /dev/null
    while [ $? -ne 0 ]; do
        sleep 1
        xec > /dev/null
    done

    # Run the script that will bind each of the passthrough graphics
    # cards to pciback.
    . ${VG_SHARE}/discrete_gfx_modprobe.sh

    # Determine the gpus that the UIVM needs to start on
    GPUS=$(xec -o /host get-displayhandler-gpu)
    if [ ${#GPUS} -gt 0 ]; then
        # IFS=',' read -ra GPU <<< "$GPUS"
        # for i in "${GPU[@]}"; do
        #   DEVICE="${DEVICE:-""} --devices ${i}"
        # done
        DEVICE="--devices ${GPUS}"
    fi

    #... and start VGlass.
    start-stop-daemon --start --background --pidfile "${PIDFILE}" --make-pidfile --name "${NAME}" --exec "${EXEC}" -- ${DEVICE:-""} "${OPTS}" "${OPTS_2}"
}

function stop {
    if [ -f ${PIDFILE} ]; then
        # Don't want this script exiting if this fails
        set +e
        echo -n "  Trying to stop ${NAME1} - "
        start-stop-daemon --stop --pidfile "${PIDFILE}" --name "${NAME1}" $*
        # --remove-pidfile doesn't work in our environment, so do it the hard way
        if [ $? -eq 0 ]; then
            rm ${PIDFILE}
        fi
        set -e
        if [ -f ${PIDFILE} ]; then
            echo -n "  Trying to stop ${NAME2} - "
            start-stop-daemon --stop --pidfile "${PIDFILE}" --name "${NAME2}" $*
            # --remove-pidfile doesn't work in our environment, so do it the hard way
            if [ $? -eq 0 ]; then
                rm ${PIDFILE}
            fi
        fi
    fi
}

function restart {
    stop --retry 30
    sleep 3
    start
}

do_start() {
    start-stop-daemon --start --background --quiet \
        --pidfile "${PIDFILE}" --make-pidfile --oknodo \
        --exec "${EXEC}" -- ${OPTS}
}
do_stop() {
    start-stop-daemon --stop --quiet --oknodo --pidfile "${PIDFILE}"
	rm -f "${PIDFILE}"
}

case "$1" in
    start)
        echo "Starting ${DESC}"
        do_start
        ;;
    stop)
        echo "Stopping ${DESC}"
        do_stop
        ;;
    restart)
        echo "Restarting ${DESC}"
        do_stop
        do_start
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit 0
